﻿using System;
using System.Collections.Generic;

namespace CSharpSegmenter
{
    public abstract class Segment
    {
        abstract public int PixelCount { get; }
        abstract public List<double> ColourMeans { get; }
        abstract public List<double> StandardDeviations { get; }
        abstract public HashSet<Coordinate> Coordinates { get; }
        abstract public HashSet<Coordinate> NeighboursCoordinates { get; }

        abstract public List<double> SumOfSquaredDeviations(List<double> means);

        // Determine the cost of merging the given segments: 
        // equal to the standard deviation of the combined the segments minus the sum of the standard deviations of the individual segments, 
        // weighted by their respective sizes and summed over all colour bands
        public double MergeCost(Segment segmentB)
        {
            List<double> segAStdDev = StandardDeviations;
            List<double> segBStdDev = segmentB.StandardDeviations;

            Segment combined = new Parent(this, segmentB);
            List<double> combinedStdDev = combined.StandardDeviations;

            double cost = 0.0;
            for (int i = 0; i < combinedStdDev.Count; i++)
            {
                cost += combinedStdDev[i] * combined.PixelCount - (segAStdDev[i] * PixelCount + segBStdDev[i] * segmentB.PixelCount);
            }

            return cost;
        }
    }

    public class Pixel : Segment
    {
        private Coordinate coord;
        private List<byte> colour;
        private List<double> colourMeans;
        private HashSet<Coordinate> neighbourCoordinates;

        public Pixel(Coordinate coordinate, List<byte> colour)
        {
            this.coord = coordinate;
            this.colour = colour;
            this.colourMeans = colour.ConvertAll(i => (double)i);

            // Work out neighbour coordinates
            int x = coordinate.X;
            int y = coordinate.Y;
            this.neighbourCoordinates = new HashSet<Coordinate>
            {
                new Coordinate(x - 1, y),
                new Coordinate(x + 1, y),
                new Coordinate(x, y - 1),
                new Coordinate(x, y + 1)
            };
        }

        public override int PixelCount { get { return 1; } }

        public override List<double> ColourMeans { get { return colourMeans; } }

        public override List<double> StandardDeviations { get { return new List<double>(new double[colour.Count]); } }

        public override HashSet<Coordinate> Coordinates { get { return new HashSet<Coordinate> { coord }; } }

        public override HashSet<Coordinate> NeighboursCoordinates { get { return neighbourCoordinates; } }

        public override List<double> SumOfSquaredDeviations(List<double> means)
        {
            List<double> deviations = new List<double>(); // List of 0.0's
            for (int i = 0; i < means.Count; i++)
            {
                deviations.Add(Math.Pow(means[i] - colour[i], 2));
            }
            return deviations;
        }
    }

    public class Parent : Segment
    {
        private Segment child1;
        private Segment child2;
        private int pixelCount;
        private List<double> colourMeans;
        private List<double> standardDeviations;
        private HashSet<Coordinate> coordinates;
        private HashSet<Coordinate> neighbourCoordinates;

        public Parent(Segment child1, Segment child2)
        {
            this.child1 = child1;
            this.child2 = child2;

            pixelCount = child1.PixelCount + child2.PixelCount;

            // Use mean and pixel count of children to calculate mean of parent
            List<double> child1Means = child1.ColourMeans;
            List<double> child2Means = child2.ColourMeans;
            colourMeans = new List<double>();
            for (int i = 0; i < child1Means.Count; i++)
            {
                colourMeans.Add((child1Means[i] * child1.PixelCount + child2Means[i] * child2.PixelCount) / pixelCount);
            }

            // Use means to calculate standard deviation
            List<double> squaredDeviations = SumOfSquaredDeviations(colourMeans);
            standardDeviations = new List<double>();
            for (int i = 0; i < squaredDeviations.Count; i++)
            {
                standardDeviations.Add(Math.Pow(squaredDeviations[i] / pixelCount, 0.5));
            }

            // Merge coordinates of children to get parent coordinates
            coordinates = new HashSet<Coordinate>(child1.Coordinates);
            coordinates.UnionWith(child2.Coordinates);

            // Merge neighbour coordinates of children and remove parent coordinates to get parent neighbours
            neighbourCoordinates = new HashSet<Coordinate>(child1.NeighboursCoordinates);
            neighbourCoordinates.UnionWith(child2.NeighboursCoordinates);
            neighbourCoordinates.ExceptWith(coordinates);
        }

        public override int PixelCount { get { return pixelCount; } }

        public override List<double> ColourMeans { get { return colourMeans; } }

        public override List<double> StandardDeviations { get { return standardDeviations; } }

        public override HashSet<Coordinate> Coordinates { get { return coordinates; } }

        public override HashSet<Coordinate> NeighboursCoordinates { get { return neighbourCoordinates; } }

        public override List<double> SumOfSquaredDeviations(List<double> means)
        {
            List<double> child1DeviationTotals = child1.SumOfSquaredDeviations(means);
            List<double> child2DeviationTotals = child2.SumOfSquaredDeviations(means);
            List<double> combinedDeviationToals = new List<double>();
            for (int i = 0; i < means.Count; i++)
            {
                combinedDeviationToals.Add(child1DeviationTotals[i] + child2DeviationTotals[i]);
            }
            return combinedDeviationToals;
        }
    }
}