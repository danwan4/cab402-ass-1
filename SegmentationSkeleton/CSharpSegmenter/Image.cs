﻿using System;
using System.Collections.Generic;
using BitMiracle.LibTiff.Classic;

namespace CSharpSegmenter
{
    public class Image
    {
        private int width;
        private int height;
        private int[] raster;

        private const int BLUE = unchecked((int)0xFFFF0000);

        // Create a new Image from given parameters
        public Image(int width, int height, int[] raster)
        {
            this.width = width;
            this.height = height;
            this.raster = raster;
        }

        // Create a new Image by loading an existing tiff file using BitMiracle Tiff library for .NET
        public Image(string filename)
        {
            Tiff tiff = Tiff.Open(filename, "r");
            width = tiff.GetField(TiffTag.IMAGEWIDTH)[0].ToInt();
            height = tiff.GetField(TiffTag.IMAGELENGTH)[0].ToInt();
            raster = new int[width * height];
            tiff.ReadRGBAImage(width, height, raster);
        }

        // Create a new Image with the (top left corner of the) original image but with the segment boundaries overlayed in blue
        public Image(Image originalImage, Segmentation segmentation, int N)
        {
            width = 1 << N;
            height = 1 << N;
            raster = new int[width * height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (x == width - 1 || x == 0 || segmentation.FindRoot(x, y) != segmentation.FindRoot(x - 1, y) ||
                        y == height - 1 || y == 0 || segmentation.FindRoot(x, y) != segmentation.FindRoot(x, y - 1))
                    {
                        SetColour(x, y, BLUE);
                    }
                    else
                    {
                        SetColour(x, y, originalImage.GetColour(x, y));
                    }
                }
            }
        }

        // Write Image to file using BitMiracle Tiff library for .NET
        public void SaveImage(string filename)
        {
            Tiff file = Tiff.Open(filename, "w");

            // Set image properties
            file.SetField(TiffTag.IMAGEWIDTH, width);
            file.SetField(TiffTag.IMAGELENGTH, height);
            file.SetField(TiffTag.SAMPLESPERPIXEL, 4);
            file.SetField(TiffTag.COMPRESSION, Compression.LZW);
            file.SetField(TiffTag.BITSPERSAMPLE, 8);
            file.SetField(TiffTag.ROWSPERSTRIP, 1);
            file.SetField(TiffTag.ORIENTATION, Orientation.BOTLEFT);
            file.SetField(TiffTag.PLANARCONFIG, PlanarConfig.CONTIG);
            file.SetField(TiffTag.PHOTOMETRIC, Photometric.RGB);

            byte ALPHA = 0xFF; // 255

            // Write rows to the image file
            for (int y = 0; y < height; y++)
            {
                // Create an array of bytes encoding of the given row
                byte[] row = new byte[width * 4];
                for (int x = 0; x < width; x++)
                {
                    List<byte> colour = GetColourBands(x, y);
                    // RGBA Order
                    for (int band = 0; band < colour.Count; band++)
                    {
                        row[x * 4 + band] = colour[band];
                    }
                    row[x * 4 + colour.Count] = ALPHA;
                }
                file.WriteScanline(row, y);
            }

            file.Close();
        }

        private void SetColour(int x, int y, int colour)
        {
            raster[y * width + x] = colour;
        }

        public int GetColour(int x, int y)
        {
            return raster[y * width + x];
        }

        public int GetColour(Coordinate coord)
        {
            return GetColour(coord.X, coord.Y);
        }

        public List<Byte> GetColourBands(int x, int y)
        {
            int abgr = GetColour(x, y);
            return new List<byte>
            {
                (byte)Tiff.GetR(abgr),
                (byte)Tiff.GetG(abgr),
                (byte)Tiff.GetB(abgr)
            };
        }

        public List<Byte> GetColourBands(Coordinate coord)
        {
            return GetColourBands(coord.X, coord.Y);
        }
    }
}
