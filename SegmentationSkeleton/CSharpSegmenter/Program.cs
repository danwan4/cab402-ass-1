﻿using System;
using System.Diagnostics;

namespace CSharpSegmenter
{
    class Program
    {
        static void Main(string[] args)
        {
            String imagePath = "C:\\Users\\DanZay\\Documents\\Repositories\\cab402-ass-1\\SegmentationSkeleton\\TestImages\\L15-3792E-1717N-Q4.tif";

            Image image = new Image(imagePath);

            // Testing using sub-image of size 32x32 pixels
            int N = 5;

            // Increasing this threshold will result in more segment merging and therefore fewer final segments
            double threshold = 800.0;

            // Start the timer
            Stopwatch timer = System.Diagnostics.Stopwatch.StartNew();

            // Determine the segmentation for the (top left corner of the) image (2^N x 2^N) pixels
            Segmentation seg = new Segmentation(image, N, threshold);

            // Stop and read the timer
            timer.Stop();
            Console.WriteLine("Segmentation growth time - {0}ms", timer.ElapsedMilliseconds);

            // Draw the (top left corner of the) original image but with the segment boundaries overlayed in blue
            Image segmentedImage = new Image(image, seg, N);

            segmentedImage.SaveImage("segmented.tif");

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
