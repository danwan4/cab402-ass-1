﻿using System;
using System.Collections.Generic;

namespace CSharpSegmenter
{
    public class Segmentation
    {
        private const double Epsilon = 1e-7;

        private int N;
        private int sideLength;
        private double mergeThreshold;
        private Image image;
        private Segment[,] segmentMap;
        private Dictionary<Segment, Segment> segmentation;

        public Segmentation(Image image, int N, double mergeThreshold)
        {
            this.N = N;
            this.sideLength = 1 << N; // 2^N
            this.mergeThreshold = mergeThreshold;
            this.image = image;

            // Populate the segment map
            segmentMap = new Segment[sideLength, sideLength];
            for (int x = 0; x < sideLength; x++)
            {
                for (int y = 0; y < sideLength; y++)
                {
                    segmentMap[x, y] = new Pixel(new Coordinate(x, y), image.GetColourBands(x, y));
                }
            }

            // Grow the segmentation
            segmentation = new Dictionary<Segment, Segment>();
            GrowUntilNoChange();
        }

        // Try to grow the segments corresponding to every pixel on the image in turn 
        // (considering pixel coordinates in special dither order)
        private void GrowUntilNoChange()
        {
            bool growing;
            do
            {
                growing = false;
                foreach (Coordinate coord in Dither.DitherCoordinates(N))
                {
                    growing = growing || TryGrowCoordinate(coord);
                }
            }
            while (growing);
        }

        // Try to grow the coordinate's root segment
        private bool TryGrowCoordinate(Coordinate coord)
        {
            Segment segmentA = FindRoot(coord);
            return TryGrowSegment(segmentA);
        }

        // Try to find a neighbouring segmentB such that:
        //     1) segmentB is one of the best neighbours of segment A, and 
        //     2) segmentA is one of the best neighbours of segment B
        // if such a mutally optimal neighbour exists then merge them,
        // otherwise, choose one of segmentA's best neighbours (if any) and try to grow it instead (gradient descent)
        private bool TryGrowSegment(Segment segmentA)
        {
            HashSet<Segment> segmentABestNeighbours = FindBestNeighbours(segmentA);

            bool grown = false;

            if (segmentABestNeighbours.Count > 0)
            {
                Segment[] segmentBs = new Segment[segmentABestNeighbours.Count];
                segmentABestNeighbours.CopyTo(segmentBs);
                Segment segmentB = segmentBs[0];

                HashSet<Segment> segmentBBestNeighbours = FindBestNeighbours(segmentB);

                if (segmentBBestNeighbours.Contains(segmentA))
                {
                    AddPairToSegmentation(segmentA, segmentB);
                    grown = true;
                }
                else          
                {
                    // Gradient descent
                    grown = TryGrowSegment(segmentB);
                }
            }

            return grown;
        }

        // Find the neighbour(s) of the given segment that has the (equal) best merge cost
        // (exclude neighbours if their merge cost is greater than the threshold)
        private HashSet<Segment> FindBestNeighbours(Segment segmentA)
        {
            HashSet<Segment> bestNeighbours = new HashSet<Segment>();
            double bestCost = mergeThreshold;

            HashSet<Segment> neighbours = FindNeighbours(segmentA);

            foreach (Segment segmentB in neighbours)
            {
                double mergeCost = segmentA.MergeCost(segmentB);

                if (mergeCost < bestCost)
                {
                    bestNeighbours.Clear();
                    bestNeighbours.Add(segmentB);
                    bestCost = mergeCost;
                }
                else if (mergeCost == bestCost)
                {
                    bestNeighbours.Add(segmentB);
                }
            }

            return bestNeighbours;
        }

        // Find the neighbour(s) of the given segment
        private HashSet<Segment> FindNeighbours(Segment segment)
        {
            HashSet<Coordinate> filteredNeighbourCoordinates = new HashSet<Coordinate>(segment.NeighboursCoordinates);
            filteredNeighbourCoordinates.RemoveWhere(IsCoordOutOfRange);

            HashSet<Segment> neighbours = new HashSet<Segment>();
            foreach (Coordinate coord in filteredNeighbourCoordinates)
            {
                neighbours.Add(FindRoot(coord));
            }

            return neighbours;
        }

        // Returns whether coord is outside the limits of the segmentation
        private bool IsCoordOutOfRange(Coordinate coord)
        {
            return coord.X < 0 || coord.X >= sideLength || coord.Y < 0 || coord.Y >= sideLength;
        }

        // Find the largest/top level segment that the given segment is a part of (based on the current segmentation)
        public Segment FindRoot(Segment segment)
        {
            if (segmentation.TryGetValue(segment, out Segment parentSegment))
            {
                return FindRoot(parentSegment);
            }
            else
            {
                return segment;
            }
        }

        public Segment FindRoot(int x, int y)
        {
            return FindRoot(segmentMap[x, y]);
        }

        public Segment FindRoot(Coordinate coord)
        {
            return FindRoot(coord.X, coord.Y);
        }

        // Add the merged parent of two segments to the segmentation dictionary
        private void AddPairToSegmentation(Segment segmentA, Segment segmentB)
        {
            Segment parent = new Parent(segmentA, segmentB);
            segmentation.Add(segmentA, parent);
            segmentation.Add(segmentB, parent);
        }
    }
}