﻿module Program

[<EntryPoint>]
let main argv =
    // load a Tiff image
    //let image = TiffModule.loadImage argv.[0]
    let image = TiffModule.loadImage "C:\\Users\\DanZay\\Documents\\Repositories\\cab402-ass-1\\SegmentationSkeleton\\TestImages\\L15-3792E-1717N-Q4.tif"

    // testing using sub-image of size 32x32 pixels
    let N = 5

    // increasing this threshold will result in more segment merging and therefore fewer final segments
    let threshold = 800.0

    // start the timer
    let stopWatch = System.Diagnostics.Stopwatch.StartNew()

    // determine the segmentation for the (top left corner of the) image (2^N x 2^N) pixels
    let segmentation = SegmentationModule.segment image N threshold

    // stop and read the timer
    stopWatch.Stop()
    printfn "Segmentation growth time - %fms" stopWatch.Elapsed.TotalMilliseconds

    // draw the (top left corner of the) original image but with the segment boundaries overlayed in blue
    TiffModule.overlaySegmentation image "segmented.tif" N segmentation

    System.Console.WriteLine "Press any key to exit..."
    System.Console.ReadKey() |> ignore

    0 // return an integer exit code