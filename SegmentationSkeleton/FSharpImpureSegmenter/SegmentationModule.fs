﻿module SegmentationModule

open SegmentModule

// Maps segments to their immediate parent segment that they are contained within (if any) 
type Segmentation = Map<Segment, Segment>


// Mutable dictionary that stores the last known root of each segment so that findRoot can skip recursions
let mutable lastKnownRoots = new System.Collections.Generic.Dictionary<Segment, Segment>();

// Find the largest/top level segment that the given segment is a part of (based on the current segmentation)
let findRoot (segmentation: Segmentation) segment : Segment =
    let rec findRootSlow (segmentation: Segmentation) segment : Segment =
        if segmentation.TryFind segment = None then segment else findRootSlow segmentation segmentation.[segment]

    let found, lastKnownRoot = lastKnownRoots.TryGetValue segment
    if found then
        let root = findRootSlow segmentation lastKnownRoot
        lastKnownRoots.Remove segment |> ignore
        lastKnownRoots.Add (segment, root)
        root
    else
        let root = findRootSlow segmentation segment
        lastKnownRoots.Add (segment, root)
        root


// Initially, every pixel/coordinate in the image is a separate Segment
// Note: this is a higher order function which given an image, 
// returns a function which maps each coordinate to its corresponding (initial) Segment (of kind Pixel)
let createPixelMap (image:TiffModule.Image) : (Coordinate -> Segment) =
    let pixelMap (coordinate:Coordinate) : Segment = 
        Pixel(coordinate, TiffModule.getColourBands image coordinate)

    pixelMap


// Find the neighbouring segments of the given segment (assuming we are only segmenting the top corner of the image of size 2^N x 2^N)
// Note: this is a higher order function which given a pixelMap function and a size N, 
// returns a function which given a current segmentation, returns the set of Segments which are neighbours of a given segment
let createNeighboursFunction (pixelMap:Coordinate->Segment) (N:int) : (Segmentation -> Segment -> Set<Segment>) =
    // image dimension limit (2^N)
    let limit = pown 2 N

    // return a set of the coordinates of the four surrounding pixels (left, right, above, below)
    let neighbourCoordinates (coordinate:Coordinate) =
        let x, y = coordinate
        Set.ofList [(x - 1, y); (x + 1, y); (x, y - 1); (x, y + 1)]

    // return a list of coordinates of every pixel in the segment
    let rec segmentCoordinates (segment:Segment) =
        match segment with
        | Pixel (coordinate, colour) -> [coordinate]
        | Parent (child1, child2) -> segmentCoordinates child1 @ segmentCoordinates child2
        
    // return a set of neighbour coordinates of every pixel in the segment
    let neighbourCoordinatesOfSegment (segment:Segment) =
        // returns a set of neighbour coordinates of a list of coordinates
        let rec neighbourCoordinatesOfList (coordinates:List<Coordinate>) =
                match coordinates with
                | [] -> Set.empty<Coordinate>
                | head::tail -> Set.union (neighbourCoordinates head) (neighbourCoordinatesOfList tail)

        neighbourCoordinatesOfList (segmentCoordinates segment)
        
    // return a filtered set of segment neighbour coordinates that excludes those in the segment or outside the image dimension limit
    let filteredNeighbourCoordinatesOfSegment (segment:Segment) =
        Set.difference (neighbourCoordinatesOfSegment segment) (Set.ofList (segmentCoordinates segment))
        |> Set.filter (fun (x, y) -> x >= 0 && x < limit && y >= 0 && y < limit)

    let createNeighbours (segmentation:Segmentation) (segment:Segment) : Set<Segment> =
        // return the root of every segment neighbour coordinate, excluding the segment itself
        Set.map (fun coordinate -> findRoot segmentation (pixelMap coordinate)) (filteredNeighbourCoordinatesOfSegment segment)
        |> Set.remove segment

    createNeighbours


// The following are also higher order functions, which given some inputs, return a function which ...


 // Find the neighbour(s) of the given segment that has the (equal) best merge cost
 // (exclude neighbours if their merge cost is greater than the threshold)
let createBestNeighbourFunction (neighbours:Segmentation->Segment->Set<Segment>) (threshold:float) : (Segmentation->Segment->Set<Segment>) =
    let getBestNeighbours (segmentation:Segmentation) (segment:Segment) : Set<Segment> =
        // get the cost of merging each segment and filter out those above the threshold
        let filteredCosts =
                neighbours segmentation segment
                |> Set.map (fun neighbour -> mergeCost neighbour segment)
                |> Set.filter (fun cost -> cost <= threshold)
                
        // if no segments below the threshold, return empty set, else return those that have the minimum merge cost
        if Set.isEmpty filteredCosts then 
            Set.empty<Segment> 
        else
            let minCost = Set.minElement filteredCosts
            neighbours segmentation segment
            |> Set.filter (fun neighbour -> (mergeCost neighbour segment) = minCost)

    getBestNeighbours


// Try to find a neighbouring segmentB such that:
//     1) segmentB is one of the best neighbours of segment A, and 
//     2) segmentA is one of the best neighbours of segment B
// if such a mutally optimal neighbour exists then merge them,
// otherwise, choose one of segmentA's best neighbours (if any) and try to grow it instead (gradient descent)
let createTryGrowOneSegmentFunction (bestNeighbours:Segmentation->Segment->Set<Segment>) (pixelMap:Coordinate->Segment) : (Segmentation->Coordinate->Segmentation) =
    // tries to grow a segment according to the given specification
    let rec tryGrow (segmentA:Segment) (segmentation:Segmentation) =
        let segmentABestNeighbours = segmentA |> bestNeighbours segmentation

        // if no best neighbours then halt and return the current segmentation, else try grow
        if Set.isEmpty segmentABestNeighbours then 
            segmentation
        else                
            // find any mutual neighbours of segment A
            let mutualNeighbours =
                segmentABestNeighbours
                |> Set.map (fun segmentB -> (segmentB, bestNeighbours segmentation segmentB)) 
                |> Set.filter (fun (segmentB, segmentBBestNeighbours) -> Set.contains segmentA segmentBBestNeighbours)
                |> Set.map (fun (segmentB, segmentBBestNeighbours) -> segmentB)
                
            // if mutual neighbours then add them to segmentation, else try grow one of segment A's best neighbours
            if not (Set.isEmpty mutualNeighbours) then 
                let segmentB = mutualNeighbours |> Set.toList |> List.head
                let parent = Parent(segmentA, segmentB)
                segmentation.Add(segmentA, parent).Add(segmentB, parent)
            else 
                tryGrow (segmentABestNeighbours |> Set.toList |> List.head) segmentation
    
    let tryGrowOneSegment (segmentation:Segmentation) (coordinate:Coordinate) : Segmentation =        
        // try grow the root segment of the given coordinate using the functions above
        let segmentA = findRoot segmentation (pixelMap coordinate)
        tryGrow segmentA segmentation

    tryGrowOneSegment


// Try to grow the segments corresponding to every pixel on the image in turn 
// (considering pixel coordinates in special dither order)
let createTryGrowAllCoordinatesFunction (tryGrowPixel:Segmentation->Coordinate->Segmentation) (N:int) : (Segmentation->Segmentation) =
    let tryGrowAll (segmentation:Segmentation) : Segmentation =
        let dither = DitherModule.coordinates N
        // apply tryGrowPixel to the segmentation with every coordinate in the dither sequence
        Seq.fold (fun seg coordinate -> tryGrowPixel seg coordinate) segmentation dither

    tryGrowAll


// Keep growing segments as above until no further merging is possible
let createGrowUntilNoChangeFunction (tryGrowAllCoordinates:Segmentation->Segmentation) : (Segmentation->Segmentation) =
    let rec grow (segmentation:Segmentation) : (Segmentation) =
        let grown = tryGrowAllCoordinates segmentation
        if grown = segmentation then segmentation else grow grown

    grow


// Segment the given image based on the given merge cost threshold, but only for the top left corner of the image of size (2^N x 2^N)
let segment (image:TiffModule.Image) (N: int) (threshold:float)  : (Coordinate -> Segment) =
    // create growUntilNoChange function based on the given image, size (N) and merge cost threshold
    let pixelMap = createPixelMap image
    let neighbours = createNeighboursFunction pixelMap N
    let bestNeighbours = createBestNeighbourFunction neighbours threshold
    let tryGrowPixel = createTryGrowOneSegmentFunction bestNeighbours pixelMap
    let tryGrowAllCoordinates = createTryGrowAllCoordinatesFunction tryGrowPixel N
    let growUntilNoChange = createGrowUntilNoChangeFunction tryGrowAllCoordinates

    // grow the image segmentation starting from empty
    let grown = growUntilNoChange Map.empty
    
    let getSegment (coordinate:Coordinate) : (Segment) =
        findRoot grown (pixelMap coordinate)

    getSegment