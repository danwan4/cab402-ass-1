﻿module SegmentModule

type Coordinate = (int * int) // x, y coordinate of a pixel
type Colour = byte list       // one entry for each colour band, typically: [red, green and blue]

type Segment = 
    | Pixel of Coordinate * Colour
    | Parent of Segment * Segment 


// return the total number of pixels that make up the given segment
let rec pixelCount (segment: Segment) : int =
    match segment with
    | Pixel (coordinate, colour) -> 1
    | Parent (child1, child2) -> pixelCount child1 + pixelCount child2


// return the sum of colour band values of every pixel in the given segment
let rec colourCount (segment: Segment) : int list =
    match segment with
    | Pixel (coordinate, colour) -> colour |> List.map int
    | Parent (child1, child2) -> List.map2 (+) (colourCount child1) (colourCount child2)
   
   
// return the mean of each colour band in the given segment
let colourMean (segment: Segment) : float list =
    let segmentPixelCount = float (pixelCount segment)
    colourCount segment |> List.map (fun x -> (float x) / segmentPixelCount)


// return the sum of deviation of each colour band in the given segment
// deviation is the difference between the pixel's value and the segment's mean of that value (squared)
let rec colourDeviationCount (segment: Segment) (mean: float list) : float list =
    match segment with
    | Pixel (coordinate, colour) -> List.map2 (-) (colour |> List.map float) mean |> List.map (fun x -> x ** 2.0)
    | Parent (child1, child2) -> List.map2 (+) (colourDeviationCount child1 mean) (colourDeviationCount child2 mean)
    

// return a list of the standard deviations of the pixel colours in the given segment
// the list contains one entry for each colour band, typically: [red, green and blue]
let stddev (segment: Segment) : float list =
    // work out the mean of the colour deviation, and then square root the result
    let segmentPixelCount = float (pixelCount segment)
    colourDeviationCount segment (colourMean segment) |> List.map (fun x -> (x / segmentPixelCount) ** 0.5)


// determine the cost of merging the given segments: 
// equal to the standard deviation of the combined the segments minus the sum of the standard deviations of the individual segments, 
// weighted by their respective sizes and summed over all colour bands
let mergeCost segment1 segment2 : float = 
    // work out individual standard deviations and then add them together, weighting them by pixel count
    let segment1PixelCount = float (pixelCount segment1)
    let segment2PixelCount = float (pixelCount segment2)
    let summedIndividualWeightedStdDev =
        List.map2 (+) 
            (stddev segment1 |> List.map (fun x -> x * segment1PixelCount))
            (stddev segment2 |> List.map (fun x -> x * segment2PixelCount))
    
    // work out standard deviations of the combined segment
    let combined = Parent(segment1, segment2)
    let combinedPixelCount = segment1PixelCount + segment2PixelCount
    let combinedWeightedStdDev = stddev combined |> List.map (fun x -> x * combinedPixelCount)

    // return the total difference between the two values calculated above
    List.map2 (-) combinedWeightedStdDev summedIndividualWeightedStdDev |> List.reduce (+)